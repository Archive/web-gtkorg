
package org.gtk.site.plugins;

import eventloop.fabric.template.*;
import eventloop.fabric.plugin.*;

public class Default extends PathPlugin {

  Template _template;

  public PathPluginQueryInfo[] query()
  {
    PathPluginQueryInfo info[] = new PathPluginQueryInfo[1];
    info[0] = new PathPluginQueryInfo();
    info[0].path = "/";
    info[0].handle_extra_path = true;
    return info;
  }

  public int process()
    throws Throwable
  {
    String template_name;
    if (getPath().equals("/"))
      template_name = "static/index";
    else 
      template_name = "static" + getPath().substring(0,getPath().length() - 1);
    
    try
    {
      _template = getTemplate(template_name);
    }
    catch (java.io.FileNotFoundException e)
    {
      return Plugin.PAGE_NOT_FOUND;
    }

    return Plugin.HAVE_CONTENT;
  }

  public String getContent()
    throws Throwable
  {
    return _template.getContent();
  }
}
