
package org.gtk.site.plugins;

import eventloop.fabric.template.*;
import eventloop.fabric.plugin.*;

public class Login extends PathPlugin {

  public PathPluginQueryInfo[] query()
  {
    PathPluginQueryInfo info[] = new PathPluginQueryInfo[1];
    info[0] = new PathPluginQueryInfo();
    info[0].path = "/login/";
    info[0].handle_extra_path = false;
    return info;
  }

  public int process()
    throws Throwable
  {
    return Plugin.HAVE_CONTENT;
  }

  public String getContent()
    throws Throwable
  {
    Template t = getTemplate("login/index");
    return t.getContent();
  }
}
