
package org.gtk.site.plugins;

import eventloop.fabric.template.*;
import eventloop.fabric.plugin.*;

import org.gtk.site.*;

public class AdminNews 
  extends GtkOrgAuthPlugin
  implements AuthRequired
{
  public PathPluginQueryInfo[] query()
  {
    PathPluginQueryInfo info[] = new PathPluginQueryInfo[1];
    info[0] = new PathPluginQueryInfo();
    info[0].path = "/admin/news/";
    info[0].handle_extra_path = true;
    return info;
  }

  public int process()
    throws Throwable
  {
    return Plugin.HAVE_CONTENT;
  }

  public String getContent()
    throws Throwable
  {
    return " wow ";
  }
}
