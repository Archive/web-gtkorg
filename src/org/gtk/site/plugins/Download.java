
package org.gtk.site.plugins;

import java.io.*;
import java.util.*;
import java.text.*;

import eventloop.fabric.template.*;
import eventloop.fabric.plugin.*;

public class Download extends PathPlugin 
{
  private String _ftp_dir_12 = "/home/ftp/pub/gtk/v1.2/";
  private String _ftp_dir_13 = "/home/ftp/pub/gtk/v1.3/";

  public PathPluginQueryInfo[] query()
  {
    PathPluginQueryInfo info[] = new PathPluginQueryInfo[1];
    info[0] = new PathPluginQueryInfo();
    info[0].path = "/download/";
    info[0].handle_extra_path = false;
    return info;
  }

  public int process()
    throws Throwable
  {
    return Plugin.HAVE_CONTENT;
  }

  public String getContent()
    throws Throwable
  {
    Template t = getTemplate("download/index");

    File glib_12 = findLatest(_ftp_dir_12, "glib");
    File gtk_12 = findLatest(_ftp_dir_12, "gtk");
    File glib_13 = findLatest(_ftp_dir_13, "glib");
    File gtk_13 = findLatest(_ftp_dir_13, "gtk");

    Date glib_12_date = new Date(glib_12.lastModified());
    Date gtk_12_date = new Date(gtk_12.lastModified());
    Date glib_13_date = new Date(glib_13.lastModified());
    Date gtk_13_date = new Date(gtk_13.lastModified());

    SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, yyyy  hh:mm:ss z");

    t.set("glib_12", glib_12.getName());
    t.set("gtk_12", gtk_12.getName());
    t.set("glib_12_date", formatter.format(glib_12_date));
    t.set("gtk_12_date", formatter.format(gtk_12_date));
    t.set("glib_12_length", (glib_12.length() / 1024) + "k");
    t.set("gtk_12_length", (gtk_12.length() / 1024) + "k");

    t.set("glib_13", glib_13.getName());
    t.set("gtk_13", gtk_13.getName());
    t.set("glib_13_date", formatter.format(glib_13_date));
    t.set("gtk_13_date", formatter.format(gtk_13_date));
    t.set("glib_13_length", (glib_13.length() / 1024) + "k");
    t.set("gtk_13_length", (gtk_13.length() / 1024) + "k");

    return t.getContent();
  }

  private File findLatest(String path, String packag)
  {
    File dir = new File(path);
    File files[] = dir.listFiles();

    long last_modified = 0;
    File file = null;

    for (int i = 0; i < files.length; i++)
      {
        if (files[i].getName().startsWith(packag)
            && last_modified < files[i].lastModified())
          {
            last_modified = files[i].lastModified();
            file = files[i];
          } 
      }

    if (file == null)
       throw new IllegalStateException("no such file: " + path + "/" + packag + "...");

    return file;
  }

}
