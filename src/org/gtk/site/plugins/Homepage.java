
package org.gtk.site.plugins;

import eventloop.fabric.template.*;
import eventloop.fabric.plugin.*;

public class Default extends PathPlugin {

  Template _template;

  public PathPluginQueryInfo[] query()
  {
    PathPluginQueryInfo info[] = new PathPluginQueryInfo[1];
    info[0] = new PathPluginQueryInfo();
    info[0].path = "/";
    info[0].handle_extra_path = false;
    return info;
  }

  public int process()
    throws Throwable
  {
    return Plugin.HAVE_CONTENT;
  }

  public String getContent()
    throws Throwable
  {
    Template t = getTemplate("homepage/index");
    Template news_story_t = getTemplate("homepage/news_story");
    StringBuffer buffer = new StringBuffer();

    NewsStories stories[] = new NewsStories.listLatest(10);
    for (int i = 0; i < stories.length; i++)
      {
        news_story_t.setValue("title", stories.getTitle()); 
        news_story_t.setValue("url", "/news/" + stories.getUrlFragment() + "/");
        news_story_t.setValue("title", stories.getSummary()); 
        buffer.append(news_story_t.getContent());
      }

    t.set("news_stories", buffer.toString());
    return t.getContent();
  }
}
