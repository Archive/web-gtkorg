
package org.gtk.site;

import eventloop.fabric.plugin.*;

abstract public class GtkOrgAuthPlugin 
  extends Plugin
  implements AuthRequired
{
  public boolean isLoggedIn()
  {
    return false;
  }
 
  public Class getLoginPluginClass()
  { 
    try
    {
      return Class.forName("org.gtk.site.plugins.Login");
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }
  }
}
