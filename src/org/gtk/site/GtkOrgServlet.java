

package org.gtk.site;

import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

import eventloop.fabric.template.*;
import eventloop.fabric.plugin.*;
import eventloop.fabric.database.*;

public class GtkOrgServlet extends FabricServlet
{
  private TemplateLoader _template_loader;

  public void init(ServletConfig config) 
      throws ServletException 
  {
    super.init(config);

    String plugin_str = getServletConfig().getInitParameter("plugins");
    String template_str = getServletConfig().getInitParameter("templates");

    if (plugin_str == null)
      System.err.println("plugin str was null");
    if (template_str == null)
      System.err.println("template str was null");

    // parse plugin_str and template_str for multiple  FIXME

    PathDispatcher dispatcher;
    String template_paths[] = new String[1];
    template_paths[0] = template_str;
    _template_loader = new TemplateLoader(template_paths, null);

    try
    {
      Dispatcher dispatchers[] = new Dispatcher[1];
      dispatchers[0] = new PathDispatcher(new PluginJarClassLoader(getClass().getClassLoader(), plugin_str));
      setDispatchers(dispatchers);
    }
    catch (Throwable ie)
    {
      System.err.println(ie.getMessage());
    }

  }

  public void setupRequest(HttpServletRequest req, 
                           HttpServletResponse res, 
                           Map data)
  {
    data.put("_fabric_template_loader", _template_loader);
  }

  public void cleanupRequest(HttpServletRequest req, 
                             HttpServletResponse res, 
                             Map data)
  {
  }

}

