
SUBDIRS=           \
	src

all: do_links
	for i in $(SUBDIRS); do \
	  (cd $$i && $(MAKE) $@) || exit; \
	done;

clean:
	for i in $(SUBDIRS); do \
	  (cd $$i && $(MAKE) $@) || exit; \
	done

do_links:
	./do_links

.PHONY: do_links
