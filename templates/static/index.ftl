%define title "GTK+ - The GIMP Toolkit"
%include site_top         standard/site_top.ftl
%include site_bottom      standard/site_bottom.ftl
%include section_header   standard/section_header.ftl
%include box_right_top    standard/box_right_top.ftl
%include box_right_bottom standard/box_right_bottom.ftl

$site_top

<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top>

%define section_title Introduction
$section_header

<p>
   GTK+ is a multi-platform toolkit for creating graphical user
   interfaces, primarily designed for the <A HREF="http://www.xfree86.org/">X Wi
ndow System</A>.
</P>
<P>
   GTK+ is free software and part of the 
   <a href="http://www.gnu.org/">GNU Project</a>. However,
   the licensing terms for GTK+, the <a
   href="http://www.gnu.org/copyleft/lesser.html">GNU LGPL</a>, allow it to be
   used by all developers, even those developing proprietary software,
   without any license fees or royalties.
<p>
   GTK+ has a C-based object-oriented architecture that allows for
   maximum flexibility, and consists of the following component
   libraries:
</P>
<ul>
<li><B>GLib</B> - Provides many useful data types, macros, type conversions,
    string utilities and a lexical scanner.
<li><B>GDK</B> - A wrapper for low-level windowing functions.
<li><B>GTK</B> - An advanced widget set.
</ul>


</td>
<td nowrap>
&nbsp;
</td>
<td valign=top>

%define box_right_title "Origins"
$box_right_top
GTK+ was initially developed for and used by the <A HREF="http://www.gimp.org">
GIMP</A>, the GNU Image Manipulation Program.  Therefore, it is named "The GIMP
Toolkit", so that the origins of the project are remembered.  Today GTK+
is used by a large number of applications, and is the toolkit used
by the GNU project's <A HREF="http://www.gnome.org"> GNOME</A> desktop.
$box_right_bottom


%define box_right_title "GTK+ 1.2"
$box_right_top
<p>
Here is some information about the current stable release
of GTK+, version 1.2:
</p>
<p>
- <A HREF="announce.html">Release Announcement</a><BR>
- <A HREF="glib-1.2-NEWS.html">GLib Changes Overview</a><BR>
- <A HREF="gtk+-1.2-NEWS.html">GTK+ Changes Overview</a><BR>
- <A HREF="gtk+-1.2-Changes.html">Upgrading Applications</a>
</p>
$box_right_bottom

</td></tr></table>

$site_bottom
