%define title "GTK+ - Mailing Lists"
%include site_top         standard/site_top.ftl
%include site_bottom      standard/site_bottom.ftl
%include section_header   standard/section_header.ftl

$site_top

<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top>

%define section_title Mailing Lists
$section_header

<P>
There are six GTK+ mailing lists.  
</P>
<P><B>gtk-list@gnome.org</B></P>
<UL>
<P>
This is the main mailing list, and 
probably the one you should ask questions about GTK+.  The topic
of this list is general topics related to GTK+, including everything
from discussion of proposed new features to questions about
compiling GTK+.
</P><P>
To subscribe to this list, send "subscribe" in the <I>subject</I> to
gtk-list-request@gnome.org or visit the 
<A HREF="http://mail.gnome.org/mailman/listinfo/gtk-list">list information
page</A>.
</P><P>
This list is <A HREF="http://mail.gnome.org/archives/gtk-list/">archived</a>.
</P>
</UL>

<P><B>gtk-app-devel-list@gnome.org</B></P>
<UL>
<P>
Discussion on this list is
oriented toward developing applications with GTK+.  This has been
a lower traffic list which is good for asking simple GTK+ questions.
</P><P>
To subscribe to this list, send "subscribe" in the <I>subject</I> to
gtk-app-devel-list-request@gnome.org or visit the 
<A HREF="http://mail.gnome.org/mailman/listinfo/gtk-app-devel-list">list information
page</A>.
</P><P>
This list is <A HREF="http://mail.gnome.org/archives/gtk-app-devel-list/">archived</a>.
</UL>

<P><B>gtk-devel-list@gnome.org</B></P>
<UL>
<P>
This list is for developers of GTK+
to discuss code.  General GTK+ questions should not be asked on this list,
as that is more appropriate for gtk-list.  Requests for new features should
<B>not</B> be sent to this list, as that is also more appropriate for
gtk-list.  
</P><P>
To subscribe to this list, send "subscribe" in the <I>subject</I> to
gtk-devel-list-request@gnome.org or visit the 
<A HREF="http://mail.gnome.org/mailman/listinfo/gtk-devel-list">list information
page</A>.
</P><P>
This list is <A HREF="http://mail.gnome.org/archives/gtk-devel-list/">archived</a>.
</UL>

<P><B>gtk-doc-list@gnome.org</B></P>
<UL>
<P>
This list is for the coordination of GTK+ documentation
efforts.
</P>
</P><P>
To subscribe to this list, send "subscribe" in the <I>subject</I> to
gtk-doc-list-request@gnome.org or visit the 
<A HREF="http://mail.gnome.org/mailman/listinfo/gtk-doc-list">list information
page</A>.
</P><P>
This list is <A HREF="http://mail.gnome.org/archives/gtk-doc-list/">archived</a>.
</UL>

<P><B>gtk-i18n-list@gnome.org</B></P>
<UL>
<P>
This list is intended for the discussion of the internationalization
and localization of GTK+ itself and of GTK+ applications or visit the 
<A HREF="http://mail.gnome.org/mailman/listinfo/gtk-i18n-list">list information
page</A>.
</P>
</P><P>
To subscribe to this list, send "subscribe" in the <I>subject</I> to
gtk-i18n-list-request@gnome.org.
</P><P>
This list is <A HREF="http://mail.gnome.org/archives/gtk-i18n-list/">archived</a>.
</UL>

<P><B>gtk-perl-list@gnome.org</B></P>
<UL>
<P>
This list is intended for the discussing the usage of GTK+ with Perl.
</P>
</P><P>
To subscribe to this list, send "subscribe" in the <I>subject</I> to
gtk-perl-list-request@gnome.org or visit the 
<A HREF="http://mail.gnome.org/mailman/listinfo/gtk-perl-list">list information
page</A>.
</P><P>
This list is <A HREF="http://mail.gnome.org/archives/gtk-perl-list/">archived</a>.
</UL>

</td></tr></table>

$site_bottom
