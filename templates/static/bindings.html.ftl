%define title "GTK+ - Language Bindings"
%include site_top         standard/site_top.ftl
%include site_bottom      standard/site_bottom.ftl
%include section_header   standard/section_header.ftl

$site_top

<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top>

%define section_title Language Bindings
$section_header

<P>
Language Bindings (or 'Wrappers') allow GTK+ to be used from other
programming languages, in the style of those languages. They are relatively
easy to create because GTK+ is designed with them in mind.
</P>

<br>&nbsp;


<table CELLPADDING=2  >
<tr>
<td></td>

<td></td>

<td ALIGN=CENTER COLSPAN="2"><b>Status</b></td>

<td></td>
</tr>

<tr>
<td><b>Language</b></td>

<td><b>Project</b></td>

<td><b>GTK+ 1.2</b></td>

<td><b>GTK+ 2.0</b></td>
</tr>

<tr>
<td></td>

<td></td>

<td></td>

<td></td>
</tr>

<tr>
<td><b>Complete:</b></td>

<td></td>

<td></td>

<td></td>
</tr>

<tr>
<td>Ada</td>

<td><a href="http://ada.eu.org/gtkada/">GtkAda</a></td>

<td>Complete</td>

<td></td>
</tr>

<tr>
<td>C++</td>

<td><a href="http://gtkmm.sourceforge.net">Gtk--</a></td>

<td>Complete</td>

<td></td>
</tr>

<tr>
<td>Perl</td>

<td><a href="ftp://ftp.gtk.org/pub/gtk/perl/">Perl/GTK+</a></td>

<td>Complete</td>

<td></td>
</tr>

<tr>
<td>Python</td>

<td><a href="ftp://ftp.gtk.org/pub/gtk/python/">Python/GTK+</a</td>

<td>Complete</td>

<td></td>
</tr>

<tr>
<td></td>

<td></td>

<td></td>

<td></td>
</tr>

<tr>
<td><b>Others:</b></td>

<td></td>

<td></td>

<td></td>
</tr>

<tr>
<td>C++</td>

<td><a href="http://www.guest.net/homepages/mmotta/VDKHome/index.htm">VDK</a></td>

<td></td>

<td></td>
</tr>

<tr>
<td></td>

<td><a href="http://www.freiburg.linux.de/~wxxt/">wxWindows/Gtk</a></td>

<td></td>

<td></td>
</tr>

<tr>
<td>Eiffel</td>

<td><a href="http://www.netlabs.net/hp/richieb/gtk_eiffel.html">eGTK</a></td>

<td></td>

<td></td>
</tr>

<tr>
<td></td>

<td><a href="http://www.eiffel.com/products/vision/page.html">EiffelVision,
GEL</a></td>

<td></td>

<td></td>
</tr>

<tr>
<td>Guile</td>

<td><a href="http://www.ping.de/sites/zagadka/guile-gtk/">guile-gtk</a></td>

<td></td>

<td></td>
</tr>

<tr>
<td>Haskell</td>

<td><a href="http://www.score.is.tsukuba.ac.jp/~chak/haskell/gtk/">GTK+/Haskell</a></td>

<td></td>

<td></td>
</tr>

<tr>
<td>JavasSript</td>

<td><a href="http://its.gsu.edu/jsg/js/JSGtk/">JSGtk</a></td>

<td></td>

<td></td>
</tr>

<tr>
<td>Objective-C</td>

<td><b><a href="ftp://ftp.gtk.org/pub/gtk/objc-gtkkit/">GTKKit</a></b></td>

<td></td>

<td></td>
</tr>

<tr>
<td></td>

<td><b><a href="ftp://ftp.gtk.org/pub/gtk/objc-gtoolkit">GToolKit</a></b></td>

<td></td>

<td></td>
</tr>

<tr>
<td></td>

<td><b><a href="ftp://ftp.gnome.org/pub/GNOME/sources/gnome-objc/">obgtk
(gnome-objc)</a></b></td>

<td></td>

<td></td>
</tr>

<tr>
<td>Objective-Caml</td>

<td><b><a href="http://cristal.inria.fr/~cuoq/mlgtk.html">MlGtk</a></b></td>

<td></td>

<td></td>
</tr>

<tr>
<td>Objective-Label</td>

<td><b><a href="http://wwwfun.kurims.kyoto-u.ac.jp/soft/olabl/lablgtk.html">LablGTK</a></b></td>

<td></td>

<td></td>
</tr>

<tr>
<td>Pascal</td>

<td><b><a href="http://tfdec1.fys.kuleuven.ac.be/~michael/fpc-linux/gtk/gtk.html">GTK+
for Free Pascal</a></b></td>

<td></td>

<td></td>
</tr>

<tr>
<td>TOM</td>

<td><b><a href="ftp://ftp.gerbil.org/pub/tom/stable/">TOM/gtk</a>
(<a href="http://www.gerbil.org/~yacc/">more info</a>)</b></td>

<td></td>

<td></td>
</tr>
</table>


</td></tr>
</table>

$site_bottom
