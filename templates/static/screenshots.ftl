%define title "GTK+ - Screenshots"
%include site_top         standard/site_top.ftl
%include site_bottom      standard/site_bottom.ftl
%include section_header   standard/section_header.ftl

$site_top

<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top>

%define section_title Screenshots
$section_header

<p>
<B>Applications</B>
</p>
<p>
The best GTK+ screenshots are from applications which are successfully
using GTK+.  Here are a few pages which show off such applications:

<UL>
<A HREF="http://www.gimp.org/the_gimp_screenshots.html">GIMP</A><BR>
<A HREF="http://www.gnome.org/seegnome.html">GNOME</A><BR>
<A HREF="http://glade.pn.org/screenshots.html">Glade</A><BR>
</UL>
</p>

<p>
<B>Ports</B>
</p>
<P>
Other interesting screenshots can be seen at projects aimed at porting
GTK+ to alternative display systems, instead of using X Windows:
<UL>
Linux-FB: 
<A HREF="http://www.lysator.liu.se/~alla/files/testgtk.png">testgtk</A>
<A HREF="http://www.lysator.liu.se/~alla/files/testtext.png">testtext</A>
<A HREF="http://www.lysator.liu.se/~alla/files/gtktetris.png">testtext</A>
<A HREF="http://www.lysator.liu.se/~alla/files/diafb.png">Dia</A><BR>
<A HREF="http://www.gtk.org/beos/screenshots/gtk-beos-testgtk-1.gif">BeOS</A>
</UL>
</p>

<p>
<B>Themes</B>
</p>
<p>
The best place to see GTK+ theme screenshots is to just browse around 
<A HREF="http://gtk.themes.org/">gtk.themes.org</A>.
</p>

</td></tr>
</table>

$site_bottom
