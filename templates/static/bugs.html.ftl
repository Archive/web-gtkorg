%define title "GTK+ - Bug Tracker"
%include site_top         standard/site_top.ftl
%include site_bottom      standard/site_bottom.ftl
%include section_header   standard/section_header.ftl

$site_top

<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top>

%define section_title Bug Tracker
$section_header

<P>
GTK+ bugs are maintained in GNOME's bug tracking system on
<A HREF="http://bugs.gnome.org/">bugs.gnome.org</A>.  The two important
packages are:
<UL>
<A HREF="http://bugs.gnome.org/db/pa/lglib.html">GLib Bugs</A><BR>
<A HREF="http://bugs.gnome.org/db/pa/lgtk+.html">GTK+ Bugs</A>
</UL>
</P>
<P>
<B>Reporting Bugs</B>
</P>
<P>
<I>(The following is taken from the <A HREF="/faq/">FAQ</A>.)</I>
</P>
<P>Bugs should be reported to the GNOME bug tracking
system (<A HREF="http://bugs.gnome.org">bugs.gnome.org</A>). To report a problem about GTK+, send
mail to submit@bugs.gnome.org.
<P>The subject of the mail should describe your problem. In the body of
the mail, you should first include a "pseudo-header" that gives the
package and version number. This should be separated by a blank line
from the actual headers.
<P>
<PRE>
Package: gtk+
Version: 1.2.0
</PRE>
  <P>Substitute 1.2.0 with the version of GTK+ that you have installed.
  <P>Then describe the bug. Include:
  <P>
  <UL>
  <LI> Information about your system. For instance:
  <UL>
  <LI> What operating system and version</LI>
  <LI> What version of X</LI>
  <LI> For Linux, what version of the C library</LI>
  </UL>

  And anything else you think is relevant.
  </LI>
  <LI> How to reproduce the bug. 

  If you can reproduce it with the testgtk program that is built in
  the gtk/ subdirectory, that will be most convenient. Otherwise,
  please include a short test program that exhibits the behavior. As
  a last resort, you can also provide a pointer to a larger piece of
  software that can be downloaded.

  (Bugs that can be reproduced within the GIMP are almost as good as
  bugs that can be reproduced in testgtk. If you are reporting a bug
  found with the GIMP, please include the version number of the GIMP
  you are using)
  </LI>
  <LI> If the bug was a crash, the exact text that was printed out
  when the crash occured.
  </LI>
  <LI> Further information such as stack traces may be useful, but are
  not necessary. If you do send a stack trace, and the error is an X
  error, it will be more useful if the stacktrace is produced running
  the test program with the <CODE>--sync</CODE> command line option.</LI>
  </UL>

</td></tr></table>

$site_bottom

