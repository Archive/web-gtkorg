%define title "GTK+ - API Documentation"
%include site_top         standard/site_top.ftl
%include site_bottom      standard/site_bottom.ftl
%include section_header   standard/section_header.ftl

$site_top

<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top>

%define section_title API Documentation
$section_header


<P>
<B>GTK+ 1.2 API Reference</B> (stable)
</P>
<P>
The following reference manuals cover the libraries in the GTK+-1.2 platform. 
</P>
<UL>
<P>
<table>
<tr><td>
<B>GLib</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/glib/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/glib-docs.tar.gz">tarball</A>)<BR>
</td></tr><tr><td>
<B>GDK</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/gdk/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/gdk-docs.tar.gz">tarball</A>)<BR>
</td></tr><tr><td>
<B>GTK</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/gtk/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/gtk-docs.tar.gz">tarball</A>)<BR>
</td></tr></table>
</P>
</UL>

<P>
<B>GTK+ 2.0 API Reference</B> (PRELIMINARY)
</P>
<P>
The following reference manuals cover the libraries in the GTK+-2.0 platform. 
</P>
<UL>
<table>
<tr>
<td> 
<B>GLib</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/2.0/glib/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/glib-2.0-docs.tar.gz">tarball</A>)<BR>
</td></tr><tr><td>
<B>GObject</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/2.0/gobject/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/gobject-2.0-docs.tar.gz">tarball</A>)<BR>
</td></tr><tr><td>
<B>Pango</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/2.0/pango/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/pango-1.0-docs.tar.gz">tarball</A>)<BR>
</td></tr><tr><td>
<B>GdkPixbuf</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/2.0/gdk-pixbuf/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/gdk-pixbuf-2.0-docs.tar.gz">tarball</A>)<BR>
</td></tr><tr><td>
<B>GDK</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/2.0/gdk/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/gdk-2.0-docs.tar.gz">tarball</A>)<BR>
</td></tr><tr><td>
<B>GTK</B> 
</td><td>
(<A HREF="http://developer.gnome.org/doc/API/2.0/gtk/index.html">online</A> | <A HREF="http://developer.gnome.org/doc/API/gtk-2.0-docs.tar.gz">tarball</A>)<BR>
</td></tr></table>
</UL>

</td></tr></table>

$site_bottom
