%include box_sidebar_top    standard/box_sidebar_top.ftl
%include box_sidebar_bottom standard/box_sidebar_bottom.ftl

<html>
<head>
<title>$title</title>
</head>
<body bgcolor="#FFFFFF">

<table cellspacing=6 border=0 cellpadding=0 width="100%">
<tr>
<td bgcolor="#FFFFFF" valign=top nowrap>
<center>
<A HREF="/"><img alt="GTK+ Logo" src="/images/gtk-logo-rgb.gif" width=107 height=140 border=0></A>
</center>
</td>
<td valign=top>
</td>
</tr>
<tr>
<td valign=top>
<BR>

%define box_sidebar_title "General"
$box_sidebar_top
<A HREF="/">Introduction</A><BR>
<A HREF="/screenshots/">Screenshots</A><br>
<A HREF="/download/">Download</A><br>
<A HREF="/mailinglists.html">Mailing Lists</A><BR>
<A HREF="/bindings.html">Language Bindings</A><BR>
<A HREF="http://gtk.themes.org/">Themes</A><BR>
<A HREF="/bugs.html">Bug Tracker</A><BR>
$box_sidebar_bottom

%define box_sidebar_title "Documentation"
$box_sidebar_top
<A HREF="/faq/">FAQ</A><br>
<A HREF="/tutorial/">Tutorial</A><BR>
<A HREF="/api/">API Reference</A><br>
<A HREF="/books.html">Published Books</A><BR>
$box_sidebar_bottom

%define box_sidebar_title "Projects"
$box_sidebar_top
<A HREF="http://www.pango.org/">Pango</A><BR>
<A HREF="http://sources.redhat.com/inti/">Inti</A><BR>
<A HREF="http://www.gnome.org/">GNOME</A><BR>
<A HREF="http://user.sgic.fi/~tml/gimp/win32/">GTK+ for Win32</A><br>
<A HREF="http://people.redhat.com/sopwith/gtkfb/">GtkFB (Framebuffer)</A><br>
<A HREF="/beos/">GTK+ for BeOS</A>
$box_sidebar_bottom

%define box_sidebar_title "Applications"
$box_sidebar_top
<A HREF="http://www.gimp.org/">GIMP</A><BR>
<A HREF="http://www.abiword.org/">Abiword</A><BR>
<A HREF="http://www.lysator.liu.se/~alla/dia/dia.html">Dia</A><BR>
<A HREF="http://glade.pn.org/">Glade</A><BR>
<A HREF="http://www.gnucash.org/">GnuCash</A><BR>
<A HREF="http://www.gnome.org/projects/gnumeric/">Gnumeric</A><BR>
<BR>
<A HREF="http://www.gnome.org/applist/">GNOME Software Map</A><br>
$box_sidebar_bottom

    </td>
  <td bgcolor="#ffffff" valign=top width="99%">

<BR>

