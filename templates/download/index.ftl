%define title "GTK+ - Download"
%include site_top         standard/site_top.ftl
%include site_bottom      standard/site_bottom.ftl
%include section_header   standard/section_header.ftl

$site_top

<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top>

%define section_title Download
$section_header


<P>
<B>Stable (v1.2)</B>
</P>

<table>
<tr>
<td nowrap>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</td>
<td>
<P>
The current stable version of GTK+ is 1.2.  
</P>
<table cellpadding="4">
<tr>
<td bgcolor="#CCCCCC" colspan="3">
<B>Source</B> (<A HREF="ftp://ftp.gtk.org/pub/gtk/v1.2/">ftp://ftp.gtk.org/pub/gtk/v1.2/)</A>
</td>
</tr><tr>
<td bgcolor="#EEEEEE">
$glib_12_date
</td>
<td bgcolor="#EEEEEE">
$glib_12_length
</td>
<td bgcolor="#EEEEEE">
<A HREF="ftp://ftp.gtk.org/pub/gtk/v1.2/$glib_12">$glib_12</A>
</td>
</tr><tr>
<td bgcolor="#EEEEEE">
$gtk_12_date
</td>
<td bgcolor="#EEEEEE">
$gtk_12_length
</td>
<td bgcolor="#EEEEEE">
<A HREF="ftp://ftp.gtk.org/pub/gtk/v1.2/$gtk_12">$gtk_12</A>
</td>
</table>
<P>
Install GLib first and GTK+ second.  Read the included
file INSTALL for specific instructions.
</P>

</td>
</tr>
</table>

<P>
<B>Development (v1.3)</B>
</P>

<table>
<tr>
<td nowrap>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</td>
<td>
<P>
The current development version of GTK+ is 1.3.  <B><I>This is probably
not what you want - this is primarily for developers and testers of 
alpha versions of GTK+</I></B>. It is also possible to get the latest 
development sources out of CVS, see below.
</P>
<P>
If you do not already have the following on your system, you will need
to download and install them as well, as they are required by GTK+ 1.3:
</P>
<table cellpadding="4">
<tr>
<td bgcolor="#CCCCCC" colspan="2">
<B>Required Dependancies for GTK+ 1.3</B>
</td>
</tr>
<tr>
<td bgcolor="#EEEEEE">
TIFF
</td>
<td bgcolor="#EEEEEE">
<A HREF="http://www.libtiff.org/">
http://www.libtiff.org/</A>
</td>
</tr>
<tr>
<td bgcolor="#EEEEEE">
libpng
</td>
<td bgcolor="#EEEEEE">
<A HREF="ftp://swrinde.nde.swri.edu/pub/png/src/">
ftp://swrinde.nde.swri.edu/pub/png/src/</A>
</td>
</tr>
<tr>
<td bgcolor="#EEEEEE">
JPEG
</td>
<td bgcolor="#EEEEEE">
<A HREF="ftp://ftp.uu.net/graphics/jpeg/">
ftp://ftp.uu.net/graphics/jpeg/</A>
</td>
</tr>
<tr>
<td bgcolor="#EEEEEE">
FriBidi
</td>
<td bgcolor="#EEEEEE">
<A HREF="http://imagic.weizmann.ac.il/~dov/freesw/FriBidi/">
http://imagic.weizmann.ac.il/~dov/freesw/FriBidi/</A>
</td>
</tr>
<tr>
<td bgcolor="#EEEEEE">
Pango
</td>
<td bgcolor="#EEEEEE">
<A HREF="http://www.pango.org/">
http://www.pango.org/</A>
</td>
</tr>
</table>

<P>
The following are the sources:
</P>
<table cellpadding="4">
<tr>
<td bgcolor="#CCCCCC" colspan="3">
<B>Source</B> (<A HREF="ftp://ftp.gtk.org/pub/gtk/v1.3/">ftp://ftp.gtk.org/pub/gtk/v1.3/)</A>
</td>
</tr><tr>
<td bgcolor="#EEEEEE">
$glib_13_date
</td>
<td bgcolor="#EEEEEE">
$glib_13_length
</td>
<td bgcolor="#EEEEEE">
<A HREF="ftp://ftp.gtk.org/pub/gtk/v1.2/$glib_13">$glib_13</A>
</td>
</tr><tr>
<td bgcolor="#EEEEEE">
$gtk_13_date
</td>
<td bgcolor="#EEEEEE">
$gtk_13_length
</td>
<td bgcolor="#EEEEEE">
<A HREF="ftp://ftp.gtk.org/pub/gtk/v1.3/$gtk_13">$gtk_13</A>
</td>
</table>
<P>
Install GLib first and GTK+ second.  Read the included
file INSTALL for specific instructions.
</P>
</td>
</tr>
</table>

<P>
<B>CVS</B>
</P>

<table>
<tr>
<td nowrap>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</td>
<td>
<P>
The latest source is always available via anonymous CVS.  The GLib and
GTK+ sources are hosted in GNOME's CVS repository as the 'glib' and
'gtk+' CVS modules.  
<A HREF="http://developer.gnome.org/tools/cvs.html">
Instructions on using GNOME's CVS repository.</A>
</P>

</td></tr>
</table>

</td>
</tr>
</table>

$site_bottom
